import torch
from torch.autograd import Variable
from torch import nn, optim
import torch.nn.functional as F

from torch.utils.data import Dataset
from torchvision import datasets, transforms

import numpy as np
from tqdm import tqdm

from dataset import SentimentDataset



class TextCNN(nn.Module):
    def __init__(self, emb, num_embeddings, embedding_dim, cnn_filter_num, max_padding_sentence_len, dropout_p):
        super(TextCNN, self).__init__()
        # padding_idx: If given, pads the output with the embedding vector at
        #              padding_idx (initialized to zeros) whenever it encounters the index.
        # 如果pytorch的Embedding没有提供这个padding的功能，第一时间想到的不应该是扩展Embedding使它加上这个功能吗？
        # self.emb = nn.Embedding(num_embeddings, embedding_dim, padding_idx=0)
        self.emb = emb
        self.conv_w2 = nn.Sequential(
                            nn.Conv1d(embedding_dim, cnn_filter_num, 2),
                            # nn.BatchNorm1d(64),
                            nn.ReLU(),
                            nn.MaxPool1d(max_padding_sentence_len - 2 + 1))
        self.conv_w3 = nn.Sequential(
                            nn.Conv1d(embedding_dim, cnn_filter_num, 3),
                            # nn.BatchNorm1d(64),
                            nn.ReLU(),
                            nn.MaxPool1d(max_padding_sentence_len - 3 + 1))
        self.conv_w4 = nn.Sequential(
                            nn.Conv1d(embedding_dim, cnn_filter_num, 4),
                            # nn.BatchNorm1d(64),
                            nn.ReLU(),
                            nn.MaxPool1d(max_padding_sentence_len - 4 + 1))
        self.conv_w5 = nn.Sequential(
                            nn.Conv1d(embedding_dim, cnn_filter_num, 5),
                            # nn.BatchNorm1d(64),
                            nn.ReLU(),
                            nn.MaxPool1d(max_padding_sentence_len - 5 + 1))
        self.conv_w6 = nn.Sequential(
                            nn.Conv1d(embedding_dim, cnn_filter_num, 6),
                            # nn.BatchNorm1d(64),
                            nn.ReLU(),
                            nn.MaxPool1d(max_padding_sentence_len - 6 + 1))
        self.fc1 = nn.Linear(cnn_filter_num * 5, 128)
        self.dropout = nn.Dropout(p=dropout_p)
        self.out = nn.Linear(128, 5)

    def forward(self, x):
        # import pdb; pdb.set_trace()
        batch_size = x.size(0)
        x = self.emb(x)
        x = x.permute(0, 2, 1)
        # x = x.view(batch_size, 1, *x.shape[1:])
        x_w2 = self.conv_w2(x)
        x_w3 = self.conv_w3(x)
        x_w4 = self.conv_w4(x)
        x_w5 = self.conv_w5(x)
        x_w6 = self.conv_w6(x)
        x = torch.cat([x_w2, x_w3, x_w4, x_w5, x_w6], dim=1)
        x = x.view(batch_size, -1)
        x = self.fc1(x)
        x = self.dropout(x)
        x = F.relu(x)
        x = self.out(x)
        # 我用的是CrossEntropyLoss, 所以这里不需要用softmax
        # x = F.softmax(x)
        return x


def train(model, criterion, optimizer, dataloader, epochs, verbose=False, fn=None):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)
    criterion = criterion.to(device)
    def _train(fn):
        for epoch in range(epochs):
            total_loss = 0
            for i,(inputs,labels) in tqdm(enumerate(dataloader)):
                model.zero_grad()
                # import pdb; pdb.set_trace()
                inputs = inputs.to(device)
                labels = labels.to(device)

                loss = fn(model, criterion, inputs, labels)

                loss.backward()
                optimizer.step()

                total_loss += loss.item() # / inputs.size(0)
                if verbose:
                    if i % 100 == 0:
                        print(f"epoch: {epoch}, iter: {i}, loss: {total_loss}")
            print(f"epoch: {epoch}, loss: {total_loss}")
        return model
    def _deco(fn):
        def __deco():
            return _train(fn)
        return __deco
    if fn: return _train(fn)
    return _deco



# TODO: 当batch_size取到16的时候会出现 "NNPACK SpatialConvolution_updateOutput failed", NMD! 为什么?
if __name__ == "__main__":
    lr = 0.0015
    batch_size = 128
    epochs = 10
    # 我用glove.200d
    embedding_dim = 200
    cnn_filter_num = 128
    # 对于这个问题,计算得到avg_sentence_len为11,我向上取到16
    max_padding_sentence_len = 24
    dropout_p = 0.5

    def sent_collate_fn(batch):
        inputs, labels = [], []
        for data in batch:
            inputs.append(data[0])
            labels.append(data[1])
        #for i,inp in enumerate(inputs):
        #    if len(inp) < max_padding_sentence_len:
        #       inp.extend([0] * (max_padding_sentence_len - len(inp)))
        #    elif len(inp) > max_padding_sentence_len:
        #        inputs[i] = inp[:max_padding_sentence_len]
        #if len(inputs) == 0:
        #   inputs.append([0])
        return (torch.tensor(inputs), torch.tensor(labels))

    traindata = SentimentDataset(max_padding_sentence_len=max_padding_sentence_len, train=True)
    testdata = SentimentDataset(max_padding_sentence_len=max_padding_sentence_len, train=False)
    train_loader = torch.utils.data.DataLoader(dataset=traindata, batch_size=batch_size, shuffle=True, collate_fn=sent_collate_fn)
    test_loader = torch.utils.data.DataLoader(dataset=testdata, batch_size=1, shuffle=False, collate_fn=sent_collate_fn)

    emb = nn.Embedding(traindata.dictionary_size, embedding_dim)
    # import pdb; pdb.set_trace()
    emb.weight = nn.Parameter(torch.tensor(traindata.vec))

    net = TextCNN(emb, traindata.dictionary_size, embedding_dim, cnn_filter_num, traindata.max_padding_sentence_len, dropout_p)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=lr)
    # optimizer = optim.SGD(net.parameters(), lr=lr, momentum=0.8)

    @train(net, criterion, optimizer, train_loader, epochs, verbose=False)
    def sent_train(model, criterion, inputs, labels):
        y_predict = model(inputs)
        loss = criterion(y_predict, labels)
        return loss


    def test(model_location):
        out = open("data/submission.random_embeddings_with_text_cnn.csv", "w+")
        out.write("PhraseId,Sentiment\n")
        base_idx = 156061
        device = torch.device("cuda")
        net = torch.load(model_location).to(device)
        with torch.no_grad():
            for idx,(inputs, label) in tqdm(enumerate(test_loader)):
                label = label.item()
                if label != -1:
                    out.write(f"{base_idx+idx},{label}\n")
                    continue

                inputs = inputs.to(device)
                y_predicted = net(inputs)
                _, label = torch.max(y_predicted, dim=1)
                out.write(f"{base_idx+idx},{label.item()}\n")
        out.close()

    sent_train()
    torch.save(net, "text_cnn.torch")
    test("text_cnn.torch")
    # mnist_test()