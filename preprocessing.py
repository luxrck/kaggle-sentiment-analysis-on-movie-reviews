import gensim
from gensim import corpora, models

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer

from tqdm import tqdm

import pandas

import torch
from torch.utils.data import Dataset
import torch.nn.functional as F

from torchvision import transforms

import numpy as np
import pickle

stemmer = SnowballStemmer("english")
stopwords = []
stopwords.extend(['.', ',', '"', "'", ':', ';', '(', ')', '[', ']', '{', '}'])

def sent2words(sent):
    # words = nltk.word_tokenize(sent.lower())
    words = sent.lower().split()
    words = [w for w in words if w not in stopwords]
    return words

def computeDictionary():
    train = pandas.read_csv("data/train.tsv", sep="\t")
    test = pandas.read_csv("data/test.tsv", sep="\t")

    sentences_train = []
    sentences_test = []

    max_sentence_len = 0

    for i,row in tqdm(train.iterrows()):
        words = sent2words(row.Phrase)
        if len(words) > max_sentence_len:
            max_sentence_len = len(words)
        if len(words) > 0:
            sentences_train.append((words, row.Sentiment))

    for i,row in tqdm(test.iterrows()):
        label = -1
        words = sent2words(row.Phrase.lower())
        if len(words) > max_sentence_len:
            max_sentence_len = len(words)
        if len(words) == 0:
            words = ["a"]
            label = 2
        sentences_test.append((words, label))

    sentences = [p[0] for p in sentences_train + sentences_test]
    dictionary = corpora.Dictionary(sentences)

    word_lens = [len(sent) for sent in sentences]

    # %%
    # 为何使用这个公式?为何这样的句子长度比较有效?有没有其它方案了?
    max_padding_sentence_len = int(np.round(np.mean(word_lens) + 4*np.std(word_lens)))

    return sentences_train, sentences_test, dictionary, max_padding_sentence_len, max_sentence_len

sentences_train, sentences_test, dictionary, suggested_padding_sentence_len, max_sentence_len = computeDictionary()

words = [v[1] for v in dictionary.items()]
# words = pd.read_csv("data/words.csv", sep="\t")
# words = list(words.Word)

gloveVec = {}
gv = open("glove/glove.6B.200d.txt")
for row in tqdm(gv):
    row = row.split()
    gloveVec[row[0]] = [float(n) for n in row[1:]]

wordVec = []
# 对于idx==0:我定义一个全0的向量.因为之后我要把这个当成nn.Embedding的weight。
wordVec.append(list(np.zeros(200)))
for w in tqdm(words):
    if w in gloveVec:
        wordVec.append(gloveVec[w])
    else:
        print("not in fasttext:", w)
        vec = list(np.random.randn(200))
        gloveVec[w] = vec
        wordVec.append(vec)

def sent2bow(s):
    a = dictionary.doc2idx(s)[:suggested_padding_sentence_len]
    a.extend([0] * (suggested_padding_sentence_len - len(s)))
    return a

bow_train = [(sent2bow(s[0]), s[1]) for s in tqdm(sentences_train)]
bow_test = [(sent2bow(s[0]), s[1]) for s in tqdm(sentences_test)]

out = {
    "words": words,
    "vec": wordVec,
    # "sentences_train": sentences_train,
    # "sentences_test": sentences_test,
    "bow_train": bow_train,
    "bow_test": bow_test,
    "dictionary": dictionary,
    "max_padding_sentence_len": suggested_padding_sentence_len,
    "max_sentence_len": max_sentence_len,
    }
pickle.dump(out, open("data/preprocessing.pkl", "wb+"))