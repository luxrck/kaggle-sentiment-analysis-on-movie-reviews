from tqdm import tqdm

import torch
from torch.utils.data import Dataset
import torch.nn.functional as F

from torchvision import transforms

import numpy as np
import pickle

config = pickle.load(open("data/preprocessing.pkl", "rb"))
embedding_weight = torch.tensor(config["vec"])
# 我们选取0代表空的Embedding向量
class SentimentDataset(Dataset):
    def __init__(self, max_padding_sentence_len=-1, train=True):
        self.isTrain = train
        self.vec = config["vec"]
        #self.sentences_train = config["sentences_train"]
        #self.sentences_test = config["sentences_test"]
        self.bow_train = config["bow_train"]
        self.bow_test = config["bow_test"]
        self.dictionary = config["dictionary"]
        self.dictionary_size = len(self.dictionary) + 1
        self.max_sentence_len = config["max_sentence_len"]
        self.max_padding_sentence_len = config["max_padding_sentence_len"]
        if max_padding_sentence_len != -1:
            self.max_padding_sentence_len = max_padding_sentence_len

    def __len__(self):
        return len(self.bow_train if self.isTrain else self.bow_test)

    def __getitem__(self, ix):
        unit = self.bow_train[ix] if self.isTrain else self.bow_test[ix]
        # sentVec = []
        label = unit[1]
        #if len(unit[0]):
        #    sentVec = self.dictionary.doc2idx(unit[0])
        #else:
        #    label = 2
        # import pdb; pdb.set_trace()
        #sentVec = list(map(lambda x:x+1, sentVec))[:self.max_padding_sentence_len]
        #sentVec.extend([0] * (self.max_padding_sentence_len - len(sentVec)))
        # import pdb; pdb.set_trace()
        # sentVec = torch.tensor(sentVec)
        # sentVec = F.pad(sentVec, (0, max_sentence_len - sentVec.size(0)))
        # import pdb; pdb.set_trace()

        return unit[0], label
