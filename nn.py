import torch
from torch.autograd import Variable
from torch import nn, optim
import torch.nn.functional as F

from torch.utils.data import Dataset
from torchvision import datasets, transforms

import numpy as np

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
import pandas as pd

import gensim
from gensim import corpora, models

from xgboost.sklearn import XGBClassifier

from tqdm import tqdm



#
# Features
#



# BOW(Bag of Words)
class BOW(object):
    def __init__(self, sentences):
        vocab = {}
        avaliable_index = 1
        for line in sentences:
            for w in set(line):
                if not vocab.get(w, None):
                    vocab[w] = avaliable_index; avaliable_index += 1
        self.vocab = vocab
        self.size = len(vocab)

    def __call__(self, sentence):
        vec = np.zeros(self.size)
        for w in sentence:
            idx = vec.get(w, -1)
            if idx == -1: continue
            vec[idx-1] += 1
        return vec


def multiclass_logloss(actual, predicted, eps=1e-15):
    """对数损失度量（Logarithmic Loss  Metric）的多分类版本。
    :param actual: 包含actual target classes的数组
    :param predicted: 分类预测结果矩阵, 每个类别都有一个概率
    """
    # Convert 'actual' to a binary array if it's not already:
    if len(actual.shape) == 1:
        actual2 = np.zeros((actual.shape[0], predicted.shape[1]))
        for i, val in enumerate(actual):
            actual2[i, val] = 1
        actual = actual2

    clip = np.clip(predicted, eps, 1 - eps)
    rows = actual.shape[0]
    vsota = np.sum(actual * np.log(clip))
    return -1.0 / rows * vsota


def tf_idf_with_logistic_regression():
    traindata = pd.read_csv("data/train.tsv", sep="\t")
    testdata = pd.read_csv("data/test.tsv", sep="\t")

    tf_vectorizer = TfidfVectorizer()
    tf_vectorizer.fit(list(traindata.Phrase) + list(testdata.Phrase))
    x_train = tf_vectorizer.transform(traindata.Phrase)
    y_train = traindata.Sentiment

    x_test = tf_vectorizer.transform(testdata.Phrase)

    optimizer = LogisticRegression(solver="lbfgs", multi_class="multinomial", max_iter=10000)
    optimizer.fit(x_train, y_train)

    predicted = optimizer.predict_proba(x_test)

    out = open("data/submission.tf_idf_with_logistic_regression.csv", "w+")
    out.write("PhraseId,Sentiment\n")
    idx = 156061
    for yp in predicted:
        out.write(f"{idx},{np.argmax(yp)+1}\n")
        idx += 1
    out.close()


def word2vec_with_logistic_regression():
    traindata = pd.read_csv("data/train.tsv", sep="\t")
    testdata = pd.read_csv("data/test.tsv", sep="\t")

    wvec = gensim.models.Word2Vec(list(traindata.Phrase) + list(testdata.Phrase), min_count=3, window=5, size=100)

    # [%0001%]
    # word2vec是一种词向量表示方式。用它来表示句向量的时候，大多数情况下，我们仅仅将句子里的每个词向量相加再求平均，得到的结果即为句向量。
    # 真是简单粗暴。。。
    def sent2vec(sent):
        return np.average([wvec[word] for word in sent], axis=0)

    x_train = [sent2vec(sent) for sent in traindata.Phrase]
    y_train = traindata.Sentiment
    # import pdb; pdb.set_trace()
    x_test = [sent2vec(sent) for sent in testdata.Phrase]

    optimizer = LogisticRegression(solver="lbfgs", multi_class="multinomial", max_iter=10000)
    optimizer.fit(x_train, y_train)

    predicted = optimizer.predict_proba(x_test)

    out = open("data/submission.word2vec_with_logistic_regression.csv", "w+")
    out.write("PhraseId,Sentiment\n")
    idx = 156061
    for yp in predicted:
        out.write(f"{idx},{np.argmax(yp)}\n")
        idx += 1
    out.close()


def word2vec_with_xgboost():
    traindata = pd.read_csv("data/train.tsv", sep="\t")
    testdata = pd.read_csv("data/test.tsv", sep="\t")

    wvec = gensim.models.Word2Vec(list(traindata.Phrase) + list(testdata.Phrase), min_count=3, window=5, size=100)

    def sent2vec(sent):
        return np.average([wvec[word] for word in sent], axis=0)

    x_train = np.array([sent2vec(sent) for sent in traindata.Phrase])
    y_train = traindata.Sentiment
    # import pdb; pdb.set_trace()
    x_test = np.array([sent2vec(sent) for sent in testdata.Phrase])

    # TODO: xgboost的参数了解一下？
    xgb_params = {
        "max_depth": 5,
        "learning_rate": 0.1,
        "n_estimators": 100,
        "nthread": 6,
    }
    optimizer = XGBClassifier(**xgb_params)

    print("Start XGB Training...")
    optimizer.fit(x_train, y_train)

    predicted = optimizer.predict_proba(x_test)

    out = open("data/submission.word2vec_with_xgboost.csv", "w+")
    out.write("PhraseId,Sentiment\n")
    idx = 156061
    for yp in predicted:
        out.write(f"{idx},{np.argmax(yp)}\n")
        idx += 1
    out.close()


def glove_embeddings_load(glove_file):
    glove = {}
    glove_cnt = {}
    embeddings_dim = 0
    with open(glove_file, 'r') as g:
        for line in g:
            word_em = line.split()
            if not embeddings_dim:
                embeddings_dim = len(word_em) - 1
            word = word_em[0]
            glove[word] = [float(v) for v in word_em[1:]]
            glove_cnt.setdefault(word, 0)
            glove_cnt[word] += 1
    return glove, glove_cnt, embeddings_dim
class Glove(object):
    def __init__(self, glove_file, min_count=3):
        vocab, vocab_cnt, emb_dim = glove_embeddings_load(glove_file)
        self.vocab = vocab
        self.vocab_cnt = vocab_cnt
        self.min_count = min_count
        self.embedding_dim = emb_dim
    def __getitem__(self, k):
        val = self.vocab.get(k, [])
        if k in self.vocab:
            if self.vocab_cnt[k] < self.min_count:
                return np.zeros(self.embedding_dim)
        # 如果单词没有对应的embedding,我们就给它随机分配个embedding
        # 判断其是否为空貌似不能使用`not val`的形式
        else:
            val = np.random.randn(self.embedding_dim)
            self.vocab[k] = val
            self.vocab_cnt.setdefault(k, self.min_count)
        return self.vocab[k]


def glove_pretrained_with_xgboost():
    traindata = pd.read_csv("data/train.tsv", sep="\t")
    testdata = pd.read_csv("data/test.tsv", sep="\t")

    #wvec = gensim.models.Word2Vec(list(traindata.Phrase) + list(testdata.Phrase), min_count=3, window=5, size=100)
    wvec = Glove("glove/glove.6B.300d.txt", min_count=2)
    # import pdb; pdb.set_trace()
    def sent2vec(sent):
        words = [w for w in sent.split()]
        if len(words) == 0:
            return np.zeros(wvec.embedding_dim)
        return np.average([wvec[w] for w in words], axis=0)

    x_train = np.array([sent2vec(sent) for sent in traindata.Phrase])
    y_train = traindata.Sentiment
    # import pdb; pdb.set_trace()
    x_test = np.array([sent2vec(sent) for sent in testdata.Phrase])

    xgb_params = {
        "max_depth": 5,
        "learning_rate": 0.1,
        "n_estimators": 300,
        "nthread": 6,
    }
    optimizer = XGBClassifier(**xgb_params)

    print("Start XGB Training...")
    optimizer.fit(x_train, y_train)

    predicted = optimizer.predict_proba(x_test)

    out = open("data/submission.glove_pretrained_with_xgboost.csv", "w+")
    out.write("PhraseId,Sentiment\n")
    idx = 156061
    for yp in predicted:
        out.write(f"{idx},{np.argmax(yp)}\n")
        idx += 1
    out.close()


class SentimentLinearNet(nn.Module):
    def __init__(self, in_dim, hidden_dim, out_dim):
        super(SentimentLinearNet, self).__init__()
        self.fc1 = nn.Linear(in_dim, hidden_dim)
        torch.nn.init.xavier_uniform(self.fc1.weight, gain=1)
        self.fc2 = nn.Linear(hidden_dim, out_dim)
        torch.nn.init.xavier_uniform(self.fc2.weight, gain=1)
    def forward(self, x):
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)
        # x = F.softmax(x)
        return x
class SentimentGRUNet(nn.Module):
    def __init__(self, in_dim, rnn_hidden_dim, out_dim):
        super(SentimentGRUNet, self).__init__()
        self.gru = nn.GRU()
        self.out = nn.Linear(rnn_hidden_dim, out_dim)
        self.nn.init.xavier_uniform_(self.out.weight, gain=1)
    def forward(self, x):
        pass
class GloveDataset(Dataset):
    def __init__(self, glove, fname, train=True):
        self.glove = glove
        self.data = pd.read_csv(fname, sep="\t")
        self.is_train = train
        # self.transform = transforms.Compose([transforms.ToTensor()])
    def __len__(self):
        return len(self.data)
    def __getitem__(self, ix):
        # import pdb; pdb.set_trace()
        line = self.data.iloc[ix, :].values
        sent = line[2]

        def sent2vec(sent):
            words = [w for w in sent.split()]
            if len(words) == 0:
                return np.zeros(self.glove.embedding_dim)
            return np.average([self.glove[w] for w in words], axis=0)
        sentVec = sent2vec(sent)

        label = -1
        if self.is_train:
            label = line[3]
        #labelVec = np.zeros(5); labelVec[label] = 1
        return (label, torch.tensor(sentVec, dtype=torch.float32).view(1,-1))

def glove_pretrained_with_nn():
    lr = 0.1
    epoch = 50
    batch_size = 100
    glove_words_min_count = 2

    glove = Glove("glove/glove.6B.300d.txt", min_count=glove_words_min_count)
    traindata = GloveDataset(glove, "data/train.tsv")
    testdata = GloveDataset(glove, "data/test.tsv", train=False)

    trainloader = torch.utils.data.DataLoader(dataset=traindata, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(dataset=testdata, batch_size=1, shuffle=False)

    net = SentimentLinearNet(300, 200, 5)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=lr)

    def train(save_location):
        for e in range(epoch):
            total_loss = 0
            for i, (labels, inputs) in tqdm(enumerate(trainloader)):
                #labels = torch.tensor(labels, dtype=torch.float32)
                inputs = inputs.view(inputs.size()[0], -1)
                net.zero_grad()
                y_predicted = net(inputs)
                # 交叉熵损失函数会自动帮助我们把labels转换为one-hot向量.
                loss = criterion(y_predicted, labels)
                total_loss += loss
                loss.backward()
                optimizer.step()
            print(f"epoch: {e}, {total_loss}")
        torch.save(net, save_location)

    def test(model_location):
        net = torch.load(model_location)
        # import pdb; pdb.set_trace()
        out = open("data/submission.glove_pretrained_with_nn.csv", "w+")
        out.write("PhraseId,Sentiment\n")
        base_idx = 156061
        for idx,(_,inputs) in enumerate(testloader):
            inputs = inputs.view(1,-1)
            y_predicted = net(inputs)
            _, label = torch.max(y_predicted, dim=1)
            out.write(f"{base_idx+idx},{label.item()}\n")
        out.close()

    model_location = "sentiment_linear_net.torch"
    train(model_location)
    test(model_location)



#tf_idf_with_logistic_regression()
#word2vec_with_logistic_regression()
#word2vec_with_xgboost()
glove_pretrained_with_xgboost()
#glove_pretrained_with_nn()