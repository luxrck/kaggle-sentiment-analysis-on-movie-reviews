import torch
from torch.autograd import Variable
from torch import nn, optim
import torch.nn.functional as F

from torch.utils.data import Dataset
from torchvision import datasets, transforms

import numpy as np
from tqdm import tqdm

from dataset import SentimentDataset

# def sent2vec(dictionary, )



def glove_embeddings_load(glove_file):
    glove = {}
    embeddings_dim = 0
    with open(glove_file, 'r') as g:
        for line in g:
            word_em = line.split()
            if not embeddings_dim:
                embeddings_dim = len(word_em) - 1
            word = word_em[0]
            glove[word] = [float(v) for v in word_em[1:]]
    return glove, embeddings_dim


class Glove(object):
    def __init__(self, glove_file):
        vocab, emb_dim = glove_embeddings_load(glove_file)
        self.vocab = vocab
        self.embedding_dim = emb_dim
    def __getitem__(self, k):
        if not k in self.vocab:
        # 如果单词没有对应的embedding,我们就给它随机分配个embedding
        # 判断其是否为空貌似不能使用`not val`的形式
            val = list(np.random.randn(self.embedding_dim))
            self.vocab[k] = val
        return self.vocab[k]


def dataset(filename, glove, train=True):
    pd = pandas.read_csv(filename, sep="\t")
    inputs = []
    labels = []

    prev_sent_id = -1

    def vec(inputs):
        inputVecs = []
        inputLens = []

        u_len = -1

        for words in inputs:
            wvec = []

            if u_len == -1: u_len = len(words)

            inputLens.append(len(words))

            for w in words:
                wvec.append(glove[w])
            wvec = torch.tensor(wvec)
            # import pdb; pdb.set_trace()
            if u_len > len(words):
                wvec = F.pad(wvec, (0,0,0,u_len - len(words)))
            inputVecs.append(wvec.view(1, wvec.size(0), wvec.size(1)))
        # import pdb; pdb.set_trace()
        return torch.cat(inputVecs), inputLens

    for ix in range(len(pd)):
        row = pd.iloc[ix,:].values
        if row[1] != prev_sent_id:
            if prev_sent_id == -1:
                prev_sent_id = row[1]; continue
            try:
                # import pdb; pdb.set_trace()
                yield labels, vec(sorted(inputs, key=lambda x: len(x), reverse=True))
            except Exception as e:
                import pdb; pdb.set_trace()
            prev_sent_id = row[1]
            inputs.clear()
            labels.clear()
        words = row[2].split()
        if len(words):
            inputs.append(words)
            # 缩进一下，逻辑通顺
            if train:
                labels.append(row[3])

    return labels, vec(inputs)


def test_dataset(filename, glove):
    def sent2vec(sent):
        words = sent.split()
        if not words: words = [' ']
        return torch.tensor([glove[w] for w in words]), [len(words)]
    pd = pandas.read_csv(filename, sep="\t")
    for ix in range(len(pd)):
        row = pd.iloc[ix,:].values
        yield sent2vec(row[2])


class SentimentGRUNet(nn.Module):

    def __init__(self, num_embeddings, embedding_dim, rnn_hidden_dim, out_dim, dropout=0.4):
        super(SentimentGRUNet, self).__init__()
        self.emb = nn.Embedding(num_embeddings=num_embeddings, embedding_dim=embedding_dim)
        self.gru = nn.GRU(input_size=embedding_dim, hidden_size=rnn_hidden_dim, batch_first=True, dropout=dropout)
        self.out = nn.Linear(rnn_hidden_dim, out_dim)
        torch.nn.init.xavier_uniform_(self.out.weight, gain=1)

    def embedding(self, inputs, max_len):
        emb_x = []
        for sent in inputs:
            em = self.emb(torch.tensor(sent))
            if len(sent) < max_len:
                em = F.pad(em, (0,0,0,max_len - len(sent)))
            emb_x.append(em.view(1, em.size(0), em.size(1)))
        return torch.cat(emb_x)

    # x in shape (batch_size, len(sent))
    def forward(self, x, x_len):
        # import pdb; pdb.set_trace()
        batch_size = len(x)
        emb_x = self.embedding(x, x_len[0])
        pad_x = torch.nn.utils.rnn.pack_padded_sequence(emb_x, x_len, batch_first=True)
        # pad_x = emb_x
        pad_y, h_n = self.gru(pad_x)
        x = h_n.view(batch_size, -1)
        x = self.out(x)
        return x


def glove_pretrained_with_rnn_0():
    lr = 0.01
    epoch = 1
    # glove_words_min_count = 2
    glove_embedding_dim = 300
    rnn_hidden_dim = 200
    out_dim = 5

    # glove = Glove("glove/glove.6B.300d.txt")
    # torch.save(glove, 'glove.bin')
    glove = torch.load("glove.bin")
    testloader = test_dataset("data/test.tsv", glove)

    net = SentimentGRUNet(glove_embedding_dim, rnn_hidden_dim, out_dim)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=lr)
    def train(save_location):
        for e in range(epoch):
            total_loss = 0
            trainloader = dataset("data/train.tsv", glove)
            for i, (labels, (inputs, inputLens)) in tqdm(enumerate(trainloader)):
                labels = torch.LongTensor(labels)

                net.zero_grad()

                y_predicted = net(inputs, inputLens)
                loss = criterion(y_predicted, labels)
                total_loss += loss
                print(f"\tepoch: {e}, {loss/len(labels)}")
                loss.backward()
                optimizer.step()
            print(f"epoch: {e}, {total_loss}")
        torch.save(net, save_location)

    def test(model_location):
        net = torch.load(model_location)
        # import pdb; pdb.set_trace()
        out = open("data/submission.glove_pretrained_with_rnn.csv", "w+")
        out.write("PhraseId,Sentiment\n")
        base_idx = 156061
        with torch.no_grad():
            for idx,(inputs, inputLens) in tqdm(enumerate(testloader)):
                # inputs = inputs.view(1,-1)
                # import pdb; pdb.set_trace()
                inputs = inputs.view(1, inputs.size(0), -1)
                y_predicted = net(inputs, inputLens)
                _, label = torch.max(y_predicted, dim=1)
                out.write(f"{base_idx+idx},{label.item()}\n")
        out.close()

    print("Start trianning...")
    # train("gru.torch")
    test("rnn.torch")



def glove_pretrained_with_rnn():
    lr = 0.1
    epoch = 10
    batch_size = 100
    embedding_dim = 128
    rnn_hidden_dim = 128
    out_dim = 5
    dropout = 0.0

    traindata = SentimentDataset(train=True)
    testdata = SentimentDataset(train=False)

    num_embeddings = traindata.dictionary_size

    def sent_collate_fn(batch):
        inputs, inputLens, labels = [], [], []
        for data in batch:
            inputs.append(data[0])
            # inputLens.append(data[1])
            labels.append(data[1])
        return (inputs, labels)

    trainloader = torch.utils.data.DataLoader(dataset=traindata, batch_size=batch_size, collate_fn=sent_collate_fn, shuffle=True)
    testloader = torch.utils.data.DataLoader(dataset=testdata, batch_size=1, collate_fn=sent_collate_fn, shuffle=False)

    net = SentimentGRUNet(num_embeddings, embedding_dim, rnn_hidden_dim, out_dim, dropout)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=lr)

    def train(save_location):
        for e in range(epoch):
            total_loss = 0
            # trainloader = dataset("data/train.tsv", glove)
            for i, (inputs, labels) in tqdm(enumerate(trainloader)):
                # import pdb; pdb.set_trace()
                labels = torch.LongTensor(labels)
                inputs = sorted(inputs, key=lambda x:len(x), reverse=True)
                inputLens = [len(sentVec) for sentVec in inputs]
                # inputs = torch.tensor(inputs)

                net.zero_grad()

                y_predicted = net(inputs, inputLens)
                loss = criterion(y_predicted, labels)
                total_loss += loss
                # print(f"\tepoch: {e}, {loss/len(labels)}")
                loss.backward()
                optimizer.step()
            print(f"epoch: {e}, {total_loss}")
        torch.save(net, save_location)

    def test(model_location):
        net = torch.load(model_location)
        # import pdb; pdb.set_trace()
        out = open("data/submission.glove_pretrained_with_rnn.csv", "w+")
        out.write("PhraseId,Sentiment\n")
        base_idx = 156061
        with torch.no_grad():
            for idx,(inputs, label) in tqdm(enumerate(testloader)):
                # inputs = inputs.view(1,-1)
                # import pdb; pdb.set_trace()
                # inputs = torch.tensor(inputs)
                # inputs = inputs.view(1, inputs.size(0), -1)
                # import pdb; pdb.set_trace()
                label = label[0]
                if label != -1:
                    out.write(f"{base_idx+idx},{label}\n")
                    continue

                inputLen = len(inputs[0])
                y_predicted = net(inputs, [inputLen])
                _, label = torch.max(y_predicted, dim=1)
                out.write(f"{base_idx+idx},{label.item()}\n")
        out.close()

    print("Start trianning...")
    # train("gru.torch")
    print("Start testing...")
    test("gru.torch")


glove_pretrained_with_rnn()