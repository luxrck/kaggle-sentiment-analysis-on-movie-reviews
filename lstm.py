import torch
from torch.autograd import Variable
from torch import nn, optim
import torch.nn.functional as F

from torch.utils.data import Dataset
from torchvision import datasets, transforms

import numpy as np
from tqdm import tqdm

from dataset import SentimentDataset, embedding_weight
import re
import os
import sys



class SentimentLSTM(nn.Module):
    def __init__(self, emb_weight, num_embeddings, embedding_dim, hidden_dim, out_classes, dropout_p=0.0):
        super(SentimentLSTM, self).__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.emb.weight = nn.Parameter(emb_weight)
        self.lstm= nn.LSTM(input_size=embedding_dim, hidden_size=hidden_dim, batch_first=True, dropout=dropout_p, bidirectional=True)
        self.nn = nn.Sequential(
                    nn.Linear(2*hidden_dim, 2*hidden_dim),
                    nn.Dropout(p=dropout_p),
                    nn.ReLU(),
                    nn.Linear(2*hidden_dim, out_classes))
    def forward(self, inputs):
        # import pdb; pdb.set_trace()
        # batch_size = inputs.size(0)
        x = self.emb(inputs)
        _, (hn, cn) = self.lstm(x)
        hn = hn.permute(1,0,2).contiguous()
        hn = hn.view(hn.size(0), -1)
        y_p = self.nn(hn)
        return y_p



# We will exec `fn` immediately instead of return a wrapper function
def train(model, criterion, optimizer, dataloader, epochs, device=None, half=False, checkpoint=None, validation=None, immediate=False, verbose=False, fn=None):
    start = 0
    model_name = model.__class__.__name__

    # %%
    # 让model这么早就映射到`device`的原因是:
    # 如果我们打开`checkpoint`开关, 对于一些优化器, 我们需要加载它们之前被保存的状态,
    # 这就带来了一个问题: Optimizer::load_state_dict的实现(torch v1.1.0)是自动将state
    # 映射到`model.parameters()`所在的`device`上面, 并且将state的dtype设置成和模型参数一样的dtype.
    # 如果我们不先把model映射好, load_state之后如果我们要改变model的device的话, 会造成optimizer
    # 的参数位置和model的不在同一设备上, 即出错.
    # 所以我们必须要一开始就先把model给映射好. 并且将模型的参数类型也要设置好(比如是否用half)
    # %%
    # 那么问题来了, model.load_state_dict会不会改变model的device呢?
    # 答: 不会. model.load_state_dict加载参数最终调用的是Tensor::copy_, 这并不会改变Tensor的device.
    # 所以我们可以放心地在一开始就映射好model
    model = model.to(device)
    criterion = criterion.to(device)

    if not device:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if checkpoint:
        if checkpoint == True:
            checkpoint = "./checkpoint"
        if not os.path.exists(checkpoint):
            os.mkdir(checkpoint)
        state = None
        key = lambda x: re.search(f"(?<={model_name}\.)(\d+)(?=\.torch)", x).group(0)
        fastforward = list(filter(key, list(os.walk(checkpoint))[0][2]))
        if fastforward:
            fastforward = max(fastforward, key=lambda x: int(key(x)))
            fastforward = os.path.join(checkpoint, fastforward)
            state = torch.load(fastforward, map_location=device)
        if state:
            start = state["start"]
            model.load_state_dict(state["model"])
            # By default all the modules are initialized to train mode (self.training = True).
            # Also be aware that some layers have different behavior during train/and evaluation
            # (like BatchNorm, Dropout) so setting it matters.
            model.train()
            optimizer.load_state_dict(state["optim"])

    if half:
        model = model.half()
        criterion = criterion.half()

    def to_device(inputs, labels, device):
        if isinstance(inputs, list) or isinstance(inputs, tuple):
            inputs = torch.cat([i.view(1, *i.size()) for i in inputs], dim=0).to(device)
        inputs = inputs.to(device)
        labels = labels.to(device)
        if half and inputs.is_floating_point():
            inputs = inputs.half()
        return inputs, labels

    def _validate(fn, dataloader):
        correct, total = 0, 0
        with torch.no_grad():
            for i,(inputs,labels) in tqdm(enumerate(dataloader)):
                inputs, labels = to_device(inputs, labels, device)
                y_predict, _ = fn(model, criterion, inputs, labels)
                _, l_predict = torch.max(y_predict, dim=1)
                correct_cnt = (l_predict == labels).sum()
                total_cnt = labels.size(0)
                correct += correct_cnt.item()
                total += total_cnt
        return correct, total

    def _train(fn):
        for epoch in range(start, epochs):
            total_loss = 0
            for i,(inputs,labels) in tqdm(enumerate(dataloader)):
                model.zero_grad()
                inputs, labels = to_device(inputs, labels, device)

                _, loss = fn(model, criterion, inputs, labels)

                loss.backward()
                optimizer.step()

                total_loss += loss.item() # / inputs.size(0)
                if verbose:
                    if i % 100 == 0:
                        print(f"epoch: {epoch}, iter: {i}, loss: {total_loss}")

            if checkpoint:
                torch.save({"start": epoch + 1, "model": model.state_dict(), "optim": optimizer.state_dict()},
                            os.path.join(checkpoint, f"{model.__class__.__name__}.{epoch}.torch"))

            correct, total = 0, 0
            if validation:
                correct, total = _validate(fn, validation)
            print(f"epoch: {epoch}, loss: {total_loss}")

        return model

    def _train_wrapper(fn):
        if immediate:
            return _train(fn)
        def _train_deco():
            return _train(fn)
        return _train_deco

    if fn: return _train(fn)
    return _train_wrapper


def test(net, model_location, data):
    state = torch.load(model_location)
    net.load_state_dict(state["model"])
    net = net.cuda()
    # import pdb; pdb.set_trace()
    out = open("data/submission.glove_pretrained_with_rnn.csv", "w+")
    out.write("PhraseId,Sentiment\n")
    base_idx = 156061
    with torch.no_grad():
        for idx,(inputs, labels) in tqdm(enumerate(data)):
            label = labels[0]
            if label != -1:
                out.write(f"{base_idx+idx},{label}\n")
                continue
            inputs = inputs.cuda()
            labels = labels.cuda()
            y_predicted = net(inputs)
            _, label = torch.max(y_predicted, dim=1)
            out.write(f"{base_idx+idx},{label.item()}\n")
    out.close()

# TODO: 当batch_size取到16的时候会出现 "NNPACK SpatialConvolution_updateOutput failed", NMD! 为什么?
if __name__ == "__main__":
    lr = 0.0004
    batch_size = 32
    epochs = 2
    # 我用glove.200d
    embedding_dim = 200
    nn_hidden_dims = (400, 800, 3)
    hidden_dim = 200
    max_padding_sentence_len = 20
    dropout_p = 0.5
    out_dim = 5
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def sent_collate_fn(batch):
        p, h, labels = [], [], []
        for data in batch:
            # p.append(data[0][0])
            h.append(data[0])
        #    inputs.append((data[0], data[1]))
            labels.append(data[1])
        # import pdb; pdb.set_trace()
        return torch.tensor(h), torch.tensor(labels)

    data = SentimentDataset(train=True)
    test_data = SentimentDataset(train=False)
    # import pdb; pdb.set_trace()

    net = SentimentLSTM(emb_weight=embedding_weight, out_classes=out_dim, num_embeddings=data.dictionary_size, embedding_dim=embedding_dim, hidden_dim=hidden_dim)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=lr)

    train_loader = torch.utils.data.DataLoader(dataset=data, batch_size=batch_size, shuffle=True, collate_fn=sent_collate_fn)
    test_loader = torch.utils.data.DataLoader(dataset=test_data, batch_size=1, shuffle=False, collate_fn=sent_collate_fn)
    @train(net, criterion, optimizer, train_loader, epochs, device=device, immediate=True, checkpoint=True, verbose=False)
    def sentiment_train(model, criterion, inputs, labels):
        # inputs = torch.cat([i.view(1, *i.size()).to(device) for i in inputs], dim=0)
        y_predict = model(inputs)
        loss = criterion(y_predict, labels)
        return y_predict, loss
    test(net, f"checkpoint/{net.__class__.__name__}.{epochs-1}.torch", test_loader)
