# from bert_serving.client import BertClient

import pandas as pd
from tqdm import tqdm
import pickle
import numpy as np

# bert = BertClient()

# print("gen:outBertWordVec")
# outBertWordVec = []
# words = pd.read_csv("data/words.csv", sep="\t")
# for i,row in tqdm(words.iterrows()):
#     vec = bert.encode([row.Word])[0]
#     # 我让word_idx从1开始，我把0当成padding标志。
#     outBertWordVec.append((i+1, row.Word, vec))
# pickle.dump(outBertWordVec, open("bertWordVec.pkl", "wb+"))


# print("gen:outBertTest")
# outBertTest = []
# traindata = pd.read_csv("data/test.tsv", sep="\t")
# for i,row in tqdm(traindata.iterrows()):
#     phrase = row.Phrase.strip()
#     if not phrase:
#         sentVec = np.zeros((32, 768))
#         label = 2
#     else:
#         sentVec = bert.encode([phrase])[0]
#         label = -1
#     outBertTest.append((sentVec, label))
# pickle.dump(outBertTest, open("bertSentTest.pkl", "wb+"))


# print("gen:outBertTrain")
# outBertTrain = []
# traindata = pd.read_csv("data/train.tsv", sep="\t")
# for i,row in tqdm(traindata.iterrows()):
#     phrase = row.Phrase.strip()
#     if not phrase: continue
#     sentVec = bert.encode([phrase])[0]
#     label = row.Sentiment
#     outBertTrain.append((sentVec, label))
# pickle.dump(outBertTrain, open("bertSentTrain.pkl", "wb+"))

# import pdb; pdb.set_trace()
from dataset import *
# import pdb; pdb.set_trace()
words = [v[1] for v in dictionary.items()]
# words = pd.read_csv("data/words.csv", sep="\t")
# words = list(words.Word)

fasttextVec = {}
ft = open("glove/glove.6B.200d.txt")
# ft.readline()
print("constructe fasttext...")
for row in tqdm(ft):
    row = row.split()
    fasttextVec[row[0]] = [float(n) for n in row[1:]]

wordVec = []
for w in tqdm(words):
    if w in fasttextVec:
        wordVec.append(fasttextVec[w])
    else:
        print("not in fasttext:", w)
        wordVec.append(list(np.random.randn(300)))
wd = {"words": words, "vec": wordVec}
pickle.dump(wd, open("data/wordVec2.pkl", "wb+"))